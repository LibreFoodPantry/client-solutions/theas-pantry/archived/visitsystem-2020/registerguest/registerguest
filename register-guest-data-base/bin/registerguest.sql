-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: registerguest
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `registerguest`
--

DROP TABLE IF EXISTS `registerguest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `registerguest` (
  `id` varchar(45) NOT NULL,
  `registrationDate` varchar(45) DEFAULT NULL,
  `ageRange` int DEFAULT NULL,
  `isResident` tinyint DEFAULT NULL,
  `zipCode` varchar(45) DEFAULT NULL,
  `socialSecurity` tinyint DEFAULT NULL,
  `tanfeadc` tinyint DEFAULT NULL,
  `snap` tinyint DEFAULT NULL,
  `wic` tinyint DEFAULT NULL,
  `sfsp` tinyint DEFAULT NULL,
  `schoolBreakfast` tinyint DEFAULT NULL,
  `schoolLunch` tinyint DEFAULT NULL,
  `financialAid` tinyint DEFAULT NULL,
  `otherBenefit` varchar(45) DEFAULT NULL,
  `employed` tinyint DEFAULT NULL,
  `householdSize` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registerguest`
--

LOCK TABLES `registerguest` WRITE;
/*!40000 ALTER TABLE `registerguest` DISABLE KEYS */;
INSERT INTO `registerguest` VALUES ('0650628','04/22/2020',2,1,'02719',1,1,1,1,1,1,1,1,'ben',1,1);
/*!40000 ALTER TABLE `registerguest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'registerguest'
--

--
-- Dumping routines for database 'registerguest'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-22  9:13:24
