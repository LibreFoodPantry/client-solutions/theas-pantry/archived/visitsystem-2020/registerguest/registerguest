import {Component} from '@angular/core';
import {Registration} from './registration';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  constructor(private http: HttpClient) {
  }
  ip = 'localhost';
  port = '8080';

  title = `register-web-ui`;
  studentId = 'empty';
  studentsAgeRange = 'empty';
  zipCode = '01602';
  resident = false;
  houseHoldNumber = 'empty';
  otherBenefits = 'empty';
  none = false;
  ssi = false;
  tanfeadc = false;
  snap = false;
  wic = false;
  sfps = false;
  schoolBreakFast = false;
  schoolLunch = false;
  financialAid = false;
  employmentStatus = false;
  submit: string;
  newreg: Registration = new Registration();

  studentIdAdd(studentId: string) {
    this.studentId = studentId;
  }

  zipCodeAdd(zipCode: string) {
    this.zipCode = zipCode;
  }

  studentsAgeRangeAdd(selected: string) {
    this.studentsAgeRange = selected;
  }

  residentAdd(pressed: boolean) {
    this.resident = pressed;
  }

  employmentStatusAdd(pressed: boolean) {
    this.employmentStatus = pressed;
  }

  houseHoldNumberAdd(selected: string) {
    this.houseHoldNumber = selected;
  }

  otherBenefitsAdd(otherBenefits: string) {
    this.otherBenefits = otherBenefits;
  }

  noneAdd(checked: boolean) {
    if (checked === true) {
      this.none = true;
    }
    if (checked === false) {
      this.none = false;
    }
  }

  ssiAdd(checked: boolean) {
    if (checked === true) {
      this.ssi = true;
    }
    if (checked === false) {
      this.ssi = false;
    }
  }

  tanfeadcAdd(checked: boolean) {
    if (checked === true) {
      this.tanfeadc = true;
    }
    if (checked === false) {
      this.tanfeadc = false;
    }
  }

  snapAdd(checked: boolean) {
    if (checked === true) {
      this.snap = true;
    }
    if (checked === false) {
      this.snap = false;
    }
  }

  wicAdd(checked: boolean) {

    if (checked === true) {
      this.wic = true;
    }
    if (checked === false) {
      this.wic = false;
    }
  }

  sfpsAdd(checked: boolean) {
    if (checked === true) {
      this.sfps = true;
    }
    if (checked === false) {
      this.sfps = false;
    }
  }

  schoolBreakFastAdd(checked: boolean) {
    if (checked === true) {
      this.schoolBreakFast = true;
    }
    if (checked === false) {
      this.schoolBreakFast = false;
    }
  }

  schoolLunchAdd(checked: boolean) {
    if (checked === true) {
      this.schoolLunch = true;
    }
    if (checked === false) {
      this.schoolLunch = false;
    }
  }

  financialAidAdd(checked: boolean) {
    if (checked === true) {
      this.financialAid = true;
    }
    if (checked === false) {
      this.financialAid = false;
    }
  }

  buttonAdd() {
    if (this.resident === true) {
      this.zipCode = '01602';
    }
    if (this.errorChecker() === true) {
      this.newreg.id = this.studentId;
      this.newreg.registrationDate = '!';
      this.newreg.ageRange = Number(this.studentsAgeRange);
      this.newreg.isResident = this.resident;
      this.newreg.zipCode = this.zipCode;
      this.newreg.socialSecurity = this.ssi;
      this.newreg.tanfeadc = this.tanfeadc;
      this.newreg.snap = this.snap;
      this.newreg.wic = this.wic;
      this.newreg.sfsp = this.sfps;
      this.newreg.schoolBreakfast = this.schoolBreakFast;
      this.newreg.schoolLunch = this.schoolLunch;
      this.newreg.financialAid = this.financialAid;
      this.newreg.otherBenefit = this.otherBenefits;
      this.newreg.employed = this.employmentStatus;
      this.newreg.houseHoldSize = Number(this.houseHoldNumber);

      // send the newreg registration object over the endpoint
      this.http.post<string>('http://' + this.ip + ':' + this.port + '/registerGuest/newRegisteration',
        {
          id: this.newreg.id,
          registrationDate: this.newreg.registrationDate,
          ageRange: this.newreg.ageRange,
          isResident: this.newreg.isResident,
          zipCode: this.newreg.zipCode,
          socialSecurity: this.newreg.socialSecurity,
          tanfeadc: this.newreg.tanfeadc,
          snap: this.newreg.snap,
          wic: this.newreg.wic,
          sfsp: this.newreg.sfsp,
          schoolBreakfast: this.newreg.schoolBreakfast,
          schoolLunch: this.newreg.schoolLunch,
          financialAid: this.newreg.financialAid,
          otherBenefit: this.newreg.otherBenefit,
          employmentStatus: this.newreg.employed,
          houseHoldSize: this.newreg.houseHoldSize
        }, {responseType: 'text' as 'json'})
        .subscribe(data => {
            this.submit = data;
          },
          (error: HttpErrorResponse) => {
            this.submit = error.error;
          }
        );
    } else if (this.errorChecker() === false) {
      this.submit = 'Registration is NOT Valid';
    } else {
      this.submit = 'There is an ERROR';
    }
  }

  errorChecker(): boolean {
    if (isNaN(Number(this.studentId)) === true) {
      return false;
    }
    if (this.studentsAgeRange === 'empty') {
      return false;
    }
    if (this.zipCode.length !== 5 || isNaN(Number(this.zipCode)) === true) {
      return false;
    }
    if (this.houseHoldNumber === 'empty') {
      return false;
    }
    return true;
  }
}
